extends Node

static func draw_circle_arc(canvas, center, radius, angle_from, angle_to, color):
    var nb_points = 32
    var points_arc = Vector2Array()

    for i in range(nb_points+1):
        var angle_point = angle_from + i * (angle_to-angle_from) / nb_points - PI / 2
        var point = center + Vector2(cos(angle_point), sin(angle_point)) * radius
        points_arc.push_back( point )

    for indexPoint in range(nb_points):
        canvas.draw_line(points_arc[indexPoint], points_arc[indexPoint + 1], color, 4)