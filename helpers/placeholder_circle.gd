tool
extends Sprite

export(float) var radius = 30 setget set_radius
export(Color) var color = Color(1.0, 1.0, 1.0) setget set_color

func set_radius(val):
	radius = val
	update()

func set_color(val):
	color = val
	update()

func _draw():
	draw_circle(Vector2(0.0, 0.0), 10, color)
