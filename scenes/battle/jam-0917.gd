extends Node2D

const Character = preload("res://scenes/battle/characters/character.gd")
const Enemy = preload("res://scenes/battle/enemy/enemy.tscn")

const ncharacters = 4
const spinfactor = 0.5

onready var battlering = get_node("battlering")
onready var floors = get_node("floor").get_children()

onready var hud = get_node("hud")
onready var characters = []

var selected_character = null

func _ready():
	battlering.add_child(Enemy.instance())
	
	for i in range(ncharacters):
		var c = Character.new(i, 0, 2 * i)
		battlering.add_child(c)
		characters.append([c, hud.get_node("player" + str(i + 1))])
	
	for f in floors:
		f.play()
	
	select_character(0)
	set_process_input(true)
	set_process(true)

var left_stick = Vector2()
var reference_stick = null
func _process(delta):
	left_stick.x = Input.get_joy_axis(0, 0)
	left_stick.y = Input.get_joy_axis(0, 1)
	
	if left_stick.length() >= 0.5:
		if reference_stick != null:
			get_tree().call_group(0, "characters", "spin_cont",
									0, spinfactor * (left_stick.angle() - reference_stick.angle()))
		reference_stick = left_stick
	elif reference_stick != null:
		reference_stick = null
		get_tree().call_group(0, "characters", "snap")

func select_character(i):
	for j in range(characters.size()):
		var c = characters[j]
		if i == j:
			c[0].selected = true
			c[1].selected = true
			selected_character = c[0]
		else:
			c[0].selected = false
			c[1].selected = false

func _input(event):
	if event.is_action_pressed("inner_ring_clockwise"):
		get_tree().call_group(0, "characters", "spin",
								0, constants.CLOCKWISE)
	elif event.is_action_pressed("inner_ring_counter_clockwise"):
		get_tree().call_group(0, "characters", "spin",
								0, constants.COUNTER_CLOCKWISE)
	elif event.is_action_pressed("outer_ring_clockwise"):
		get_tree().call_group(0, "characters", "spin",
								1, constants.CLOCKWISE)
	elif event.is_action_pressed("outer_ring_counter_clockwise"):
		get_tree().call_group(0, "characters", "spin",
								1, constants.COUNTER_CLOCKWISE)
	if event.is_action_pressed("select_1"):
		select_character(0)
	elif event.is_action_pressed("select_2"):
		select_character(1)
	elif event.is_action_pressed("select_3"):
		select_character(2)
	elif event.is_action_pressed("select_4"):
		select_character(3)
	elif event.is_action_pressed("change_ring"):
		selected_character.swap_ring()