extends Sprite

const crim = preload("crim.tscn")
const blu = preload("blu.tscn")

var character_asset = null
var ring_position = 0
var ring = 0
var angular_position = 0
var type = 0
var selected = false

func _init(type, ring, ring_position):
	add_to_group("characters")
	self.type = type
	self.ring = ring
	self.ring_position = ring_position
	self.angular_position = constants.OFFSET + ring_position * constants.SLOT_DISTANCE
	position()

func _ready():
	if type == 0:
		character_asset = crim.instance()
	elif type == 1:
		character_asset = blu.instance()
	elif type == 2:
		character_asset = crim.instance()
	else:
		character_asset = blu.instance()
	add_child(character_asset)

func swap_ring():
	var next_ring = (ring + 1) % constants.RING_RADIUS.size()	
	for c in get_tree().get_nodes_in_group("characters"):
		if c.ring == next_ring and c.ring_position == ring_position:
			return
	ring = next_ring

func spin(ring, direction):
	if self.ring == ring:
		ring_position = (ring_position + direction) % constants.SLOTS
		angular_position = constants.OFFSET + ring_position * constants.SLOT_DISTANCE
		position()

func spin_cont(ring, offset):
	if self.ring == ring:
		angular_position -= offset
		position()
		
func snap():
	ring_position = int(round((angular_position - constants.OFFSET) / constants.SLOT_DISTANCE)) % constants.SLOTS
	angular_position = constants.OFFSET + ring_position * constants.SLOT_DISTANCE
	position()

func position():
	var radius = constants.RING_RADIUS[ring]
	set_pos(Vector2(cos(angular_position) * radius * constants.RING_SCALE.x, 
				sin(angular_position) * radius * constants.RING_SCALE.y))