extends Node2D

const keyboard_selectors = {
	"switch": preload("res://scenes/battle/hud/assets/controlicon_keya.png"),
	"item": preload("res://scenes/battle/hud/assets/controlicon_keyw.png"),
	"attack": preload("res://scenes/battle/hud/assets/controlicon_keyd.png"),
	"ability": preload("res://scenes/battle/hud/assets/controlicon_keys.png")
}

const gamepad_selectors = {
	"switch": preload("res://scenes/battle/hud/assets/controlicon_bt_X.png"),
	"item": preload("res://scenes/battle/hud/assets/controlicon_bt_Y.png"),
	"attack": preload("res://scenes/battle/hud/assets/controlicon_bt_B.png"),
	"ability": preload("res://scenes/battle/hud/assets/controlicon_bt_A.png")
}

onready var buttons = get_node("buttons")

func _display_selectors(gamepad):
	var selectors = gamepad_selectors 
	if not gamepad: selectors = keyboard_selectors
	
	for b in buttons.get_children():
		var k = b.get_name().split("-")[1]
		b.set_texture(selectors[k])

func _on_joy_connection_changed(device_id, connected):
	_display_selectors(connected)

func _ready():
	Input.connect("joy_connection_changed", self, "_on_joy_connection_changed")
	_display_selectors(Input.is_joy_known(0))
