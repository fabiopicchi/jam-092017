tool
extends Node2D

const fullnames = [
	"Crimson",
	"Blue",
	"Crimson",
	"Blue"
]

const portraits = [
	preload("res://scenes/battle/hud/assets/status_charactericon_01.png"),
	preload("res://scenes/battle/hud/assets/status_charactericon_02.png"),
	preload("res://scenes/battle/hud/assets/status_charactericon_01.png"),
	preload("res://scenes/battle/hud/assets/status_charactericon_02.png")
]

const keyboard_selectors = [
	preload("res://scenes/battle/hud/assets/controlicon_key1.png"),
	preload("res://scenes/battle/hud/assets/controlicon_key2.png"),
	preload("res://scenes/battle/hud/assets/controlicon_key3.png"),
	preload("res://scenes/battle/hud/assets/controlicon_key4.png")
]

const gamepad_selectors = [
	preload("res://scenes/battle/hud/assets/controlicon_dpad_top.png"),
	preload("res://scenes/battle/hud/assets/controlicon_dpad_right.png"),
	preload("res://scenes/battle/hud/assets/controlicon_dpad_left.png"),
	preload("res://scenes/battle/hud/assets/controlicon_dpad_bot.png")
]

var _typeid = 0
const _namemap = [
	"crim", 
	"blu", 
	"crim2", 
	"blu2"
]

export(Vector2) var selected_pos = Vector2(30, 0)
export(Vector2) var unselected_pos = Vector2(0, 0)

onready var selected_bg = get_node("selected_bg")
onready var fg = get_node("foreground")
onready var portrait_node = fg.get_node("portrait")
onready var selector_node = fg.get_node("selector")
onready var name_node = fg.get_node("name")
onready var hp_node = fg.get_node("hp")
onready var hp_bar = fg.get_node("hp_bar")

export(String, "crim", "blu", "crim2", "blu2") var name = "crim" setget set_name
func set_name(val):
	name = val
	_typeid = _namemap.find(val)

var hp = 100 setget set_hp	
func set_hp(val):
	hp = val
	if hp_node != null:
		hp_node.set_text(str(hp))
		
var selected = false setget set_selected
func set_selected(val):
	selected = val
	if selected:
		selected_bg.show()
		fg.set_pos(selected_pos)
	else:
		selected_bg.hide()
		fg.set_pos(unselected_pos)	
	
func _display_selector(gamepad):
	if gamepad:
		selector_node.set_texture(gamepad_selectors[_typeid])
	else:
		selector_node.set_texture(keyboard_selectors[_typeid])

func _ready():
	Input.connect("joy_connection_changed", self, "_on_joy_connection_changed")
	_display_selector(Input.is_joy_known(0))
	name_node.set_text(fullnames[_typeid])
	portrait_node.set_texture(portraits[_typeid])
	hp_node.set_text(str(hp))

func _on_joy_connection_changed(device_id, connected):
	_display_selector(connected)