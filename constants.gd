extends Node

const SLOTS = 8
const SLOT_DISTANCE = 2.0 * PI / SLOTS
const OFFSET = SLOT_DISTANCE / 2.0
const RING_RADIUS = [410.0, 600.0]
const COUNTER_CLOCKWISE = -1
const CLOCKWISE = 1
const RING_SCALE = Vector2(1.0, 0.72)